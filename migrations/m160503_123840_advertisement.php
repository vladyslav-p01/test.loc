<?php

use yii\db\Migration;

class m160503_123840_advertisement extends Migration
{
    public $tableOptions ='CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    public function up()
    {
        $this->createTable('advertisements',[
            'id' => $this->primaryKey(11),
            'title' => $this->string(255),
            'body' => $this->text(),
            'created_at' => $this->integer(),
        ], $this->tableOptions);
    }

    public function down()
    {
        $this->dropTable('advertisements');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
